# -*- coding: utf-8 -*-
# The MIT License (MIT)
#
# Copyright (c) 2015 - Enrico Spinetta (enrico.spinetta(at)gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright holders shall
# not be used in advertising or otherwise to promote the sale, use or other dealings in
# this Software without prior written authorization.

import json
from os import path
from multiprocessing import Pool, Value, Lock
import getdata
import sys

def print_progress(max, value, size):
    sp= '*' * int((float(value) / max) * size)
    print("[{}]".format(sp.ljust(size)), end='')


def myfuc(url):
    mutex.acquire()
    totalofurls.value -= 1
    print_progress(total.value, total.value - totalofurls.value, 20)
    print(" Download: {}".format(totalofurls.value))
    sys.stdout.flush()
    mutex.release()
    d = getdata.get_data(url)[0]
    d.update({'url': url})
    return d

total       = Value('i', 0)
totalofurls = Value('i', 0)
mutex = Lock()

def get_cidades():
    base='http://www.ibge.gov.br/home/geociencias/areaterritorial/area.php?nome={}'
    urls=[base.format(chr(i))for i in range(ord('A'),ord('Z'))]
    data=[]
    print("*"*60)
    print("Count={}".format(len(urls)))
    totalofurls.value = len(urls)
    total.value = len(urls)

    pool = Pool(5) # Number of connections
    m = pool.map_async(myfuc, urls)
    result = m.get(0xFFFF)

    print("*"*60)
    for r in result:
        if 'ibge' in r and len(r['ibge']) > 0:
            for k in range(len(r['ibge'])):
                data.append({
                    'cod_uf' : r['cod_uf'][k],
                    'uf'     : r['uf'][k],
                    'ibge'   : r['ibge'][k],
                    'cidade' : r['cidade'][k],
                    'area'   : r['area'][k]
                })
        else:
            print("Not found itens at {}".format(r['url']))


    with open('json_cidades.json','w') as f:
        json.dump(data, f,indent=4)

    print("*"*60)
    print("END")

if __name__ == "__main__":
    get_cidades()
