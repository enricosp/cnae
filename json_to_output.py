# -*- coding: utf-8 -*-
# The MIT License (MIT)
#
# Copyright (c) 2015 - Enrico Spinetta (enrico.spinetta(at)gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright holders shall
# not be used in advertising or otherwise to promote the sale, use or other dealings in
# this Software without prior written authorization.

import json
from lxml import etree
import dicttoxml
import sys

def new_text_node(name, value):
    node = etree.Element(name)
    node.text = value
    return node

def output_pesquisa():
    print("Start pesquisa output :",end='')
    with open('json_pesquisa.json','r') as f:
        json_pesquisa=json.load(f)

    node_root = etree.Element('items')
    dados=[]
    for d in json_pesquisa:
        print('.',end='')
        sys.stdout.flush()
        if ('internal_type' in d and 'fail_get' in d['internal_type']):
            continue
        for i in range(len(d['codes'])):
            code = d['codes'][i]
            description = d['descriptions'][i]
            dados.append({'code': code ,'description': description })
            node_code = new_text_node('description', description)
            node_code.set('code', code)
            node_root.append(node_code)

    with open('output_pesquisa.json','w') as f:
        json.dump(dados, f,indent=4)

    with open('output_pesquisa.xml','wb') as f:
        etree.ElementTree(node_root).write(f, xml_declaration=True, encoding="UTF-8", pretty_print=True)

    print("End")

def output_estrutura():
    print("Start estrutura output :",end='')
    with open('json_estrutura.json','r') as f:
        json_estrutura=json.load(f)

    count=0
    # separete by type
    s_dados={}
    for d in json_estrutura['data']:
        print('*',end='')
        sys.stdout.flush()
        if d['internal_type'] not in s_dados:
            s_dados[d['internal_type']] = []
        s_dados[d['internal_type']].append(d)


    dados = {'section': {}, "division": {} , "group": {}, "class": {}, "subclass": {}}
    k_code = {'Seção:': 'section' , 'Divisão:' : 'division', 'Grupo:': 'group', 'Classe:': 'class', 'Subclasse:': 'subclass'}
    k_code_parent = { 'section': 'root', 'division' :'section', 'group':'division', 'class':'group', 'subclass': 'class' }

    for k in ['Seção:', 'Divisão:', 'Grupo:', 'Classe:', 'Subclasse:', 'pesquisa']:
        for ds in s_dados[k]:
            print('.',end='')
            sys.stdout.flush()
            if 'pesquisa' not in k:
                count+=1
                d_type = k_code[k]
                d = { k_code[k]: ds['code'], 'type': d_type, 'parent_type': k_code_parent[d_type] }
                for inc in ['code', 'description', 'note', 'note_include', 'note_include_too', 'note_not_include', 'parent' ]:
                    if inc in ds:
                        d.update({inc: ds[inc]})
                if "subs"  in d:
                    d.update({'children_codes', []})
                    for sub in d['subs']:
                        d['children_codes'].append(sub[0])
                element={ds['code'] : d}
                dados[d_type].update(element)
                if ds['parent'].strip() and 'root' not in k_code_parent[d_type]:
                    if 'children' not in dados[k_code_parent[d_type]][ds['parent']]:
                        dados[k_code_parent[d_type]][ds['parent']].update({'children': {}})
                    dados[k_code_parent[d_type]][ds['parent']]['children'].update(element)
            else:
                if ds['parent'].strip():
                    key = 'subclass'
                    if ds['parent'] not in dados[key]:
                        key = 'class'
                    if ds['parent'] not in dados[key]:
                        raise Exception('output_estrutura', "Não encontrei")

                    if 'children_names' not in dados[key][ds['parent']]:
                        dados[key][ds['parent']].update({'children_names':[]})
                    if 'children_names_codes' not in dados[key][ds['parent']]:
                        dados[key][ds['parent']].update({'children_names_codes':[]})
                    if 'children_names_count' not in dados[key][ds['parent']]:
                        dados[key][ds['parent']].update({'children_names_count':ds['registros']})
                    dados[key][ds['parent']]['children_names'].extend(ds['descriptions'])
                    dados[key][ds['parent']]['children_names_codes'].extend(ds['codes'])


    with open('output_estrutura.json','w') as f:
        json.dump(dados['section'], f,indent=4)

    with open('output_estrutura_db.json','w') as f:
        merge={}
        for k in dados:
            merge.update(dados[k])
        json.dump(merge, f,indent=4)

    print("XML slow....")
    node_root = etree.fromstring(dicttoxml.dicttoxml(dados['section']))
    with open('output_estrutura.xml','wb') as f:
        etree.ElementTree(node_root).write(f, xml_declaration=True, encoding="UTF-8", pretty_print=True)
    print("End")
    print("Count: {}".format(count))

    topgsql_cnae(dados)

def output_cidades():
    with open('json_cidades.json','r') as f:
        json_cidades=json.load(f)

    with open('output_cidades.json','w') as f:
        json.dump(json_cidades, f,indent=4)

    node_root = etree.fromstring(dicttoxml.dicttoxml(json_cidades))
    with open('output_cidades.xml','wb') as f:
        etree.ElementTree(node_root).write(f, xml_declaration=True, encoding="UTF-8", pretty_print=True)

    topgsql_cidade(json_cidades)



def topgsql_cnae(dados, table="cnae"):
    sql='''
-- -----------------------------------------------------
-- Table {0}
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {0} (
  id VARCHAR(10) NOT NULL,
  parent VARCHAR(10) NOT NULL,
  type VARCHAR(10) NOT NULL,
  description VARCHAR(255) NOT NULL,
  others VARCHAR(255)[],
  comments TEXT,
  keywords_ts TSVECTOR,
  PRIMARY KEY(id)
);
CREATE INDEX {0}_id_idx ON {0} USING HASH(id);
CREATE INDEX {0}_parent_idx ON {0} USING HASH(parent);
CREATE INDEX {0}_keywords_ts_idx ON {0} USING gin(keywords_ts);

CREATE OR REPLACE FUNCTION {0}_ts_trigger() RETURNS trigger AS $$
BEGIN
    new.keywords_ts := setweight(to_tsvector('portuguese', coalesce(new.description, '')), 'A') ||
                       setweight(to_tsvector('portuguese', coalesce(array_to_string(new.others,' '), '')), 'B');
    return new;
END
$$ LANGUAGE plpgsql;

CREATE TRIGGER {0}_update_trigger BEFORE INSERT OR UPDATE ON {0}
    FOR EACH ROW EXECUTE PROCEDURE {0}_ts_trigger();

START TRANSACTION;

'''
    print("PostgreSQL")
    count=0
    with open('output_pgsql_cnae.sql','wb') as f:
        f.write(sql.format(table).encode('utf8'))
        for k in dados:
            for dk in dados[k]:
                count+=1
                print('.',end='')
                sys.stdout.flush()
                item = dados[k][dk]
                f.write('INSERT INTO {} VALUES (\''.format(table).encode('utf8'))
                f.write(item['code'].replace("'", "''").encode('utf8'))
                f.write(b"','")
                f.write(item['parent'].replace("'", "''").encode('utf8'))
                f.write(b"','")
                f.write(item['type'].replace("'", "''").encode('utf8'))
                f.write(b"','")
                f.write(item['description'].replace("'", "''").encode('utf8'))
                if 'children_names' in item and len(item['children_names']) > 0:
                    f.write(b"', ARRAY[")
                    f.write(",".join(["'"+v.replace("'", "''")+"'" for v in item['children_names']]).encode('utf8'))
                    f.write(b"],'")
                else:
                    f.write(b"','{}','")
                f.write(item['note'].replace("'", "''").encode('utf8'))
                f.write(b"');\n")
        f.write(b'COMMIT;\n')
    print("Count: {}".format(count))


def topgsql_cidade(dados, table="cidades"):
    sql='''
-- -----------------------------------------------------
-- Table {0}
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {0} (
  id INTEGER NOT NULL,
  description VARCHAR(40) NOT NULL,
  uf VARCHAR(2),
  ibge_uf INTEGER,
  area NUMERIC(15, 3),
  PRIMARY KEY(id)
);
CREATE INDEX {0}_id_idx ON {0} USING HASH(id);
CREATE INDEX {0}_parent_idx ON {0} USING HASH(description);
CREATE INDEX {0}_uf_idx ON {0} USING HASH(uf);

START TRANSACTION;

'''
    print("PostgreSQL cidades")
    count=0
    with open('output_pgsql_cidades.sql','wb') as f:
        f.write(sql.format(table).encode('utf8'))
        for item in dados:
            count+=1
            print('.',end='')
            sys.stdout.flush()
            f.write('INSERT INTO {} VALUES (\''.format(table).encode('utf8'))
            f.write(item['ibge'].replace("'", "''").encode('utf8'))
            f.write(b"','")
            f.write(item['cidade'].replace("'", "''").encode('utf8'))
            f.write(b"','")
            f.write(item['uf'].replace("'", "''").encode('utf8'))
            f.write(b"','")
            f.write(item['cod_uf'].replace("'", "''").encode('utf8'))
            f.write(b"','")
            f.write(item['area'].replace(",", ".").replace("'", "''").encode('utf8'))
            f.write(b"');\n")
        f.write(b'COMMIT;\n')
    print("Count: {}".format(count))

if __name__ == "__main__":
    output_pesquisa()
    output_estrutura()
    output_cidades()
