# -*- coding: utf-8 -*-
# The MIT License (MIT)
#
# Copyright (c) 2015 - Enrico Spinetta (enrico.spinetta(at)gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright holders shall
# not be used in advertising or otherwise to promote the sale, use or other dealings in
# this Software without prior written authorization.

import requests
from lxml.html import fromstring, tostring
from lxml.html.clean import Cleaner

headers = {
            'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36'
          }

transform={}
transform_check={}
transform_check['/estrutura.asp'] = lambda e: len(e['subs']) != 0
transform['/estrutura.asp'] = {
    'internal_type='   : 'Estrutura',
    'code='  : 'base',
    'subs[]' : '//*[@id="home"]/table/tr[2]/td/table/tr/td[2]/table/tr/td[1]/a'
}

transform_check['/secao.asp'] = lambda e: e['type'].strip() and e['code'].strip() and e['description'].strip()
transform['/secao.asp'] = {
    'internal_type='   : 'Seção:',
    'type'             : '(//*[@id="home"]/table/tr[2]/td/table/tr[4]/td[2]/table/tr[position()=last() or position()=last()-1]/td[1]/descendant-or-self::text())[position()=last()]',
    'code'             : '//*[@id="home"]/table/tr[2]/td/table/tr[4]/td[2]/table/tr[1]/td[2]/b/text()',
    'description'      : '//*[@id="home"]/table/tr[2]/td/table/tr[4]/td[2]/table/tr[position()=last() or position()=last()-1]/td[3]/descendant-or-self::text()[position()=last()]',
    'note'             : '/html/body/table/tr[2]/td/table/tr[contains(., "Notas Explicativas:")]/following-sibling::tr/td[2]/descendant-or-self::text()',
    'subs[]'           : '//*[@id="home"]/table/tr[2]/td/table/tr/td[2]/table/tr/td[2]/a',
}

transform_check['/divisao.asp'] = lambda e: e['type'].strip() and e['code'].strip() and e['description'].strip()
transform['/divisao.asp'] = {
    'internal_type='   : 'Divisão:',
    'type'             : '(//*[@id="home"]/table/tr[2]/td/table/tr[4]/td[2]/table/tr[position()=last() or position()=last()-1]/td[2]/descendant-or-self::text())[position()=last()]',
    'code'             : '//*[@id="home"]/table/tr[2]/td/table/tr[4]/td[2]/table/tr[2]/td[3]/descendant-or-self::text()',
    'description'      : '//*[@id="home"]/table/tr[2]/td/table/tr[4]/td[2]/table/tr[position()=last() or position()=last()-1]/td[4]/descendant-or-self::text()[position()=last()]',
    'note'             : '/html/body/table/tr[2]/td/table/tr[contains(., "Notas Explicativas:")]/following-sibling::tr/td[2]/descendant-or-self::text()',
    'subs[]'           : '//*[@id="home"]/table/tr[2]/td/table/tr/td[2]/table/tr/td[2]/a',
}

transform_check['/grupo.asp'] = lambda e: e['type'].strip() and e['code'].strip() and e['description'].strip()
transform['/grupo.asp'] = {
    'internal_type='   : 'Grupo:',
    'type'             : '/html/body/table/tr[2]/td/table/tr[3]/td[2]/table/tr[2]/td[2]/table/tr[3]/td[2]/descendant-or-self::text()',
    'code'             : '/html/body/table/tr[2]/td/table/tr[3]/td[2]/table/tr[2]/td[2]/table/tr[3]/td[3]/descendant-or-self::text()',
    'description'      : '/html/body/table/tr[2]/td/table/tr[3]/td[2]/table/tr[2]/td[2]/table/tr[3]/td[4]/descendant-or-self::text()',
    'note[:1]'         : ['/html/body/table/tr[2]/td/table/tr[contains(., "Notas Explicativas:")]/following-sibling::tr/td[2]/descendant-or-self::text()',
                         '/html/body/table/tr[2]/td[1]/table/tr[3]/td[2]/table/tr[8]/td[2]/descendant-or-self::text()'],
    'subs[]'           : '/html/body/table/tr[2]/td[1]/table/tr[3]/td[2]/table/tr[5]/td[2]/table/tr/td[2]/a',
}

transform_check['/classe.asp'] = lambda e: e['type'].strip() and e['code'].strip() and e['description'].strip()
transform['/classe.asp'] = {
    'internal_type='  : 'Classe:',
    'type'            : '//*[@id="home"]/table/tr[2]/td/table/tr[4]/td[2]/table/tr[position()=last() or position()=last()-1]/td[2]/descendant-or-self::text()[position()=last()]',
    'code'            : '//*[@id="home"]/table/tr[2]/td/table/tr[4]/td[2]/table/tr[position()=last() or position()>=last()-1]/td[3]/descendant-or-self::text()[position()=last()]',
    'description'     : '//*[@id="home"]/table/tr[2]/td/table/tr[4]/td[2]/table/tr[position()=last() or position()=last()-1]/td[4]/descendant-or-self::text()[position()=last()]',
    'note'            : '/html/body/table/tr[2]/td/table/tr[contains(., "Notas Explicativas:")]/following-sibling::tr/td[2]/descendant-or-self::text()',
    'subs[]'          : '//*[@id="home"]/table/tr[2]/td/table/tr/td[2]/table/tr/td[2]/a',
    'dolist[]'        : '//*[@id="home"]/table/tr[2]/td[1]/table/tr[4]/td[2]/table/tr[5]/td/a'
}

transform_check['/subclasse.asp'] = lambda e: e['type'].strip() and e['code'].strip() and e['description'].strip()
transform['/subclasse.asp'] = {
    'internal_type='  : 'Subclasse:',
    'type'            : '//*[@id="home"]/table/tr[2]/td/table/tr[4]/td[2]/table/tr[position()=last() or position()=last()-1]/td[2]/descendant-or-self::text()[position()=last()]',
    'code'            : '//*[@id="home"]/table/tr[2]/td/table/tr[4]/td[2]/table/tr[position()=last() or position()>=last()-1]/td[3]/descendant-or-self::text()[position()=last()]',
    'description'     : '//*[@id="home"]/table/tr[2]/td/table/tr[4]/td[2]/table/tr[position()=last() or position()=last()-1]/td[4]/descendant-or-self::text()[position()=last()]',
    'note'            : '/html/body/table/tr[2]/td/table/tr[contains(., "Notas Explicativas:")]/following-sibling::tr/td[2]/descendant-or-self::text()',
    'dolist[]'        : '//*[@id="home"]/table/tr[2]/td[1]/table/tr[4]/td[2]/table/tr[6]/td[1]/a'
}

transform_check['/pesquisa.asp'] = lambda e: e['registros'].strip() == '0' if e['registros'].strip() else False or (len(e['codes']) != 0 and len(e['descriptions']) != 0)
transform['/pesquisa.asp'] = {
    'internal_type='  : 'pesquisa',
    'registros'       : '//*[@id="home"]/table/tr[2]/td[1]/table/tr[3]/td[2]/table/tr[2]/td[2]/descendant-or-self::text()',
    'codes[]'         : '//*[@id="home"]/table/tr[2]/td/table/tr[5]/td[2]/table/tr/td[1]/descendant-or-self::text()',
    'descriptions[]'  : '//*[@id="home"]/table/tr[2]/td/table/tr[5]/td[2]/table/tr/td[2]/descendant-or-self::text()',
    'nextpage[]'      : '//*[@id="home"]/table/tr[2]/td/table/tr[1]/td/a[contains(., "Próxima")]',
    'previuspage[]'   : '//*[@id="home"]/table/tr[2]/td/table/tr[1]/td/a[contains(., "Anterior")]',
    'page'            : '//*[@id="home"]/table/tr[2]/td/table/tr[7]/td[2]/descendant-or-self::text()'
}

transform_check['areaterritorial/area.php'] = lambda e: True
transform['areaterritorial/area.php'] = {
    'internal_type='  : 'pesquisa',
    'cod_uf[]'        : '//*[@id="miolo_interno"]/table/tr[position() > 1]/td[1]/text()',
    'uf[]'            : '//*[@id="miolo_interno"]/table/tr[position() > 1]/td[2]/text()',
    'ibge[]'          : '//*[@id="miolo_interno"]/table/tr[position() > 1]/td[3]/text()',
    'cidade[]'        : '//*[@id="miolo_interno"]/table/tr[position() > 1]/td[4]/text()',
    'area[]'          : '//*[@id="miolo_interno"]/table/tr[position() > 1]/td[5]/text()',
}
transform_map={'subs[]': lambda nodes: [(node.text, node.get('href')) for node in nodes] }
transform_map['list[]'] = transform_map['subs[]']
transform_map['dolist[]'] = transform_map['subs[]']
transform_map['nextpage[]'] = transform_map['subs[]']
transform_map['previuspage[]'] = transform_map['subs[]']

def xpath_to_dict(doc, transform, mapdefs={}):
    data = {}
    def remove_spaces(s):
        if s:
            s = s.replace('&nbsp', ' ').replace('\r','').replace('\xa0', '').replace('\t','').strip()
            while '  ' in s:
                s = s.replace('  ',' ')
            s = s.replace(' \n','\n')
            while '\n\n' in s:
                s = s.replace('\n\n','\n')

        return s


    for key in transform:

        if type(transform[key]) not in (tuple, list):
            transform[key] = [transform[key]]

        if '=' in key:
            data[key[0:key.index('=')]] = transform[key][0]
            continue

        data_xpath = []
        for xp in transform[key]:
            dxp=doc.xpath(xp)
            if (len(dxp) == 0):
                continue;

            if '[:]' in key or '[:text()]' in key:
                data_xpath.append(dxp)
            else:
                data_xpath.extend(dxp)

            if '[1]' in key or '[1:text()]' in key:
                break

        datakey=key
        if '[' in key:
            datakey = key[0:key.index('[')]

        if '[]' in key or '[:]' in key or '[1]' in key:
            if key in mapdefs:
                data[datakey] = mapdefs[key](data_xpath)
            else:
                data[datakey] = data_xpath

            continue;

        mapdef=mapdefs[key] if key in mapdefs else lambda s:remove_spaces(s if type(s) == str else " ".join(s).strip())

        if '[:text()]' in key:
            data[datakey] = []
            for dxp in data_xpath:
                data[datakey].append(mapdef(dxp))
        else:
            data[datakey] = mapdef(data_xpath)

    return data

def get_data(url, times_try=5):
    global transform, transform_map
    data = {'internal_type': 'fail_get', 'url': url, 'error': '>>empty<<' }
    txt = None
    doc = None
    u,p = url.split('?',1)
    params= { k:v for k,v in [pa.split('=',1) for pa in p.split('&')] }
    for i in range(times_try):
        print("       ({}) Try download: {}".format(i, url))
        try:
            r = requests.get(u, headers=headers, timeout=20, verify=False, params=params)
            txt = r.text
            doc = fromstring(r.text)
            for key in transform:
                if key in url:
                    data = xpath_to_dict(doc, transform[key], transform_map)
                    data.update({'url': url})
                    try:
                        if not transform_check[key](data):
                            print('       # transform check fail')
                            data = {'internal_type': 'fail_get', 'url': url, 'error': 'fail transform_check', 'data': data }
                    except Exception as e:
                            print('       # transform check fail {}'.format(e))
                            data = {'internal_type': 'fail_get', 'url': url, 'error': 'fail transform_check', 'data': data }

        except Exception as e:
            data = {'internal_type': 'fail_get', 'url': url, 'error': str(e) }
            print("       {}".format(e))


        if not ('internal_type' in data and 'fail_get' in data['internal_type']):
            break


    return (data, doc, txt)
