# -*- coding: utf-8 -*-
# The MIT License (MIT)
#
# Copyright (c) 2015 - Enrico Spinetta (enrico.spinetta(at)gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright holders shall
# not be used in advertising or otherwise to promote the sale, use or other dealings in
# this Software without prior written authorization.

import json
from os import path
from multiprocessing import Pool, Value, Lock
import getdata
import sys

def print_progress(max, value, size):
    sp= '*' * int((float(value) / max) * size)
    print("[{}]".format(sp.ljust(size)), end='')


def myfuc(url):
    mutex.acquire()
    totalofurls.value -= 1
    print_progress(total.value, total.value - totalofurls.value, 20)
    print(" Download: {}".format(totalofurls.value))
    sys.stdout.flush()
    mutex.release()
    d = getdata.get_data(url[1])[0]
    d.update({'url': url})
    d.update({'parent': url[0]})
    return d

total       = Value('i', 0)
totalofurls = Value('i', 0)
mutex = Lock()

def getestrutura():
    base="https://cnae.ibge.gov.br/{0}"
    urlestrutura="https://cnae.ibge.gov.br/estrutura.asp?TabelaBusca=CNAE_202@CNAE 2.2 - Subclasses@0@cnaefiscal@0"
    json_estrutura={'urls': [('', urlestrutura)], 'data': []}
    # Carrega o json, Tenta baixar somente os que falharam
    if path.isfile('json_estrutura.json'):
        urls = []
        with open('json_estrutura.json','r') as f:
            json_estrutura=json.load(f)
        for key in range(len(json_estrutura['data'])-1,-1,-1):
            data=json_estrutura['data'][key]
            if ('internal_type' in data and 'fail_get' in data['internal_type']):
                urls.append(data['url'])
                del json_estrutura['data'][key]
        json_estrutura['urls'].extend(urls)

    urls = json_estrutura['urls']
    data = json_estrutura['data']

    while (len(urls) != 0):
        print("New interaction! Count={}".format(len(urls)))
        print("*"*60)
        totalofurls.value = len(urls)
        total.value = len(urls)

        pool = Pool(20) # Number of connections
        m = pool.map_async(myfuc, urls)
        result = m.get(0xFFFF)

        urls = []
        data += result

        for d in data:
            if not ('second' in d or ('internal_type' in d and 'fail_get' in d['internal_type'])):
                if 'subs' in d:
                    for u in d['subs']:
                        urls.append((d['code'], base.format(u[1])))
                if 'dolist' in d:
                    for u in d['dolist']:
                        urls.append((d['code'], base.format(u[1])))
                if 'nextpage' in d:
                    for u in d['nextpage']:
                        urls.append((d['parent'], base.format(u[1])))

                d.update({'second': True})

        with open('json_estrutura.json','w') as f:
            json.dump({'data': data, 'urls': urls}, f,indent=4)

        print("*"*60)
        print("END")

    fail=0
    for d in data:
        if ('internal_type' in d and 'fail_get' in d['internal_type']):
            fail+=1

    print("Fail: {}".format(fail+len(urls)))

if __name__ == "__main__":
    getestrutura()
