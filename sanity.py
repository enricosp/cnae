# -*- coding: utf-8 -*-
# The MIT License (MIT)
#
# Copyright (c) 2015 - Enrico Spinetta (enrico.spinetta(at)gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright holders shall
# not be used in advertising or otherwise to promote the sale, use or other dealings in
# this Software without prior written authorization.

import getdata
from urllib import parse
from pprint import pprint

def sanity(dourl=None, start=0, end =None):
    from urllib import parse
    _urls=['https://cnae.ibge.gov.br/estrutura.asp?TabelaBusca=CNAE_202@CNAE%202.2%20-%20Subclasses@0@cnaefiscal@0',
          'https://cnae.ibge.gov.br/secao.asp?codsecao=K&TabelaBusca=CNAE_202@CNAE%202.2%20-%20Subclasses@0@cnaefiscal@0',
          'https://cnae.ibge.gov.br/divisao.asp?coddivisao=66&CodSecao=K&TabelaBusca=CNAE_202@CNAE%202.2%20-%20Subclasses@0@cnaefiscal@0',
          'https://cnae.ibge.gov.br/grupo.asp?codgrupo=661&TabelaBusca=CNAE_202@CNAE%202.2%20-%20Subclasses@0@cnaefiscal@0',
          'https://cnae.ibge.gov.br/classe.asp?codclasse=6611-8&TabelaBusca=CNAE_202@CNAE%202.2%20-%20Subclasses@0@cnaefiscal@0',
          'https://cnae.ibge.gov.br/subclasse.asp?CodSecao=K&CodDivisao=66&CodGrupo=661&codclasse=6611-8&CodSubClasse=6611-8/01&TabelaBusca=CNAE_202@CNAE%202.2%20-%20Subclasses@0@cnaefiscal@0',
          'https://cnae.ibge.gov.br/pesquisa.asp?pesquisa=66118&TabelaBusca=CNAE_202@CNAE 2.2 - Subclasses@0@cnaefiscal@0&source=',
          'https://cnae.ibge.gov.br/pesquisa.asp?pesquisa=*&TabelaBusca=CNAE_202@CNAE 2.2 - Subclasses@0@cnaefiscal@0&source=',
          'https://cnae.ibge.gov.br/pesquisa.asp?action=proxima&intPage=613&Pesquisa=*&TipoOrdenacao=&TabelaBusca=CNAE_202@CNAE%202.2%20-%20Subclasses@0@cnaefiscal@0&SourcePage='
         ]
    urls= [parse.unquote(u) for u in _urls]
    print(urls)
    data = []
    if dourl:
        d ,_ ,_ = getdata.get_data(dourl)
        data.append(d)
    else:
        for url in urls[start:end]:
            print(url) 
            d ,_ ,_ = getdata.get_data(url)
            data.append(d)
    pprint(data)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Sanity test 1.0')
    parser.add_argument('-s','--start', help='Start url to test', default=0, type=int)
    parser.add_argument('-e','--end',   help='End url to test', default=None, type=int)
    parser.add_argument('-u','--url',   help='Test this url', default=None)
    args = parser.parse_args()
    sanity(args.url, args.start ,args.end)
