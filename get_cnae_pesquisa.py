# -*- coding: utf-8 -*-
# The MIT License (MIT)
#
# Copyright (c) 2015 - Enrico Spinetta (enrico.spinetta(at)gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright holders shall
# not be used in advertising or otherwise to promote the sale, use or other dealings in
# this Software without prior written authorization.

import json
from os import path
from multiprocessing import Pool, Value, Lock
import getdata
import sys

def myfuc(url):
    mutex.acquire()
    totalofurls.value -= 1
    print("({}<) Download: {}".format(totalofurls.value, url[1] ))
    sys.stdout.flush()
    mutex.release()
    return getdata.get_data(url)[0]

totalofurls = Value('i', 0)
mutex = Lock()

def getpesquisa():
    url_pesquisa="https://cnae.ibge.gov.br/pesquisa.asp?action=proxima&intPage={0}&Pesquisa=*&TipoOrdenacao=&TabelaBusca=CNAE_202@CNAE 2.2 - Subclasses@0@cnaefiscal@0&SourcePage="
    json_pesquisa=[]
    json_pesquisa.append(getdata.get_data(url_pesquisa.format(0))[0])
    totalreg=json_pesquisa[0]['registros']
    pages=json_pesquisa[0]['page'].split('/',1)[1]
    print("Count reg: {}".format(totalreg))
    print("Pages    : {}".format(pages))

    # Carrega o json, Tenta baixar somente os que já falharam
    if path.isfile('json_pesquisa.json'):
        url_pesquisas=[]
        with open('json_pesquisa.json','r') as f:
            json_pesquisa=json.load(f)
        for key in range(len(json_pesquisa)-1,-1,-1):
            data=json_pesquisa[key]
            if ('internal_type' in data and 'fail_get' in data['internal_type']) or ('codes' in data and len(data['codes']) == 0):
                url_pesquisas.append(data['url'])
                del json_pesquisa[key]
    else:
        url_pesquisas=[url_pesquisa.format(i) for i in range(1,int(pages))]

    totalofurls.value = len(url_pesquisas)

    pool = Pool(15) # Number of connections
    m = pool.map_async(myfuc, url_pesquisas)
    result = m.get(0xFFFF)

    with open('json_pesquisa.json','w') as f:
        json.dump(json_pesquisa + result, f,indent=4)

    regok=0
    for data in json_pesquisa:
        if 'internal_type' in data and 'fail_get' in data['internal_type']:
            continue
        if 'codes' in data and len(data['codes']) == 0:
            continue
        regok+=len(data['codes'])

    print("Summary reg: {} ok: {} fail: {}".format(totalreg,regok,int(totalreg)-regok))

if __name__ == "__main__":
    getpesquisa()
