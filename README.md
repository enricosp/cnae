# Informações importantes!!
Os dados aqui constantes foram retirados dos respectivos sites e **não foram verificados.**
Sendo que qualquer utilização destes dados ou prejuízo por utilizar esses dados **não é minha responsabilidade.**
Verifique os scripts antes de utilizar, qualquer dano causado por estes não serão de minha responsabilidade.

## CNAE (Classificação Nacional de Atividades Econômicas) e CIDADES IBGE
Salientado que esse dados e script são para ajudar na atualização de banco de dados. Retiram informações dos sites oficiais, porem os scripts não são oficiais.

1. Entendendo os scripts.

    * getdata.py              - Script responsável pelo download e transformação inicial dos dados
    * get_cnae_pesquisa.py    - Script que baixa todas as opções de pesquisa do cnae. (não utilizo no arquivo sql, opcional)
    * get_cnae_estrutura.py   - Script que baixa toda a estrutura do cnae incluindo as pesquisa nas subclasses e classes
    * get_ibge_cidades.py     - Script que baixa todas as cidades do site do ibge e suas áreas.
    * json_to_output.py       - Script converte dados brutos e cria arquivo output_*.
                                Este Script usa os dados de cache ou seja os seguintes arquivos: json_cidades.json  json_estrutura.json  json_pesquisa.json
                                Caso deseje usar este script para gerar por exemplo um SQL do MySql. Você pode baixar estes arquivos na área de download evitando assim a nesecidade da execução dos get_*
                                Aproveitado, se você criar outros formatos usando json_to_output. Ficarei contente em receber, estes script para disponibilização neste mesmo projeto.


2. Entenda os arquivos de dados.

    *  output_cidades.json        - Cidades no formato json
    *  output_cidades.xml         - Cidades no formato xml (convertido do json)  
    *  output_estrutura_db.json   - CNAE estruturado com copias dos subgrupos na raiz ideal para criar banco de dados tipo sql gerado.
    *  output_estrutura.json      - Estrutura completa do CNAE com toda a hierarquia.
    *  output_estrutura.xml       - Estrutura completa do CNAE com toda a hierarquia no formato xml convertido do json.
    *  output_pesquisa.json       - Json com termos de pesquisa
    *  output_pesquisa.xml        - XML com termos de pesquisa
    *  output_pgsql_cidades.sql   - Criação da tabela cidade com seus dados. (PostgreSQL)
    *  output_pgsql_cnae.sql      - Criação da tabela cnae com seus dados e hierarquia. (PostgreSQL)

3. Veja na area de download os arquivos já prontos e se divirta!
    Lembre-se os dados foram retirados do site oficial, mas não são oficiais. Utilize com prudencia.
